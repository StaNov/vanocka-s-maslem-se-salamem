from vanocka.Generator import Generator


def test_basic():
    result = Generator([("šroubek", "šroubku")], [], "obilí").generate()
    assert (result ==
"""Jde šroubek a potká obilí.
A obilí povídá: "Šroubku, můžu jít s tebou?"
Přičemž šroubek odpoví: "Ne".
Čas na smích.""")


def test_with_one_topping():
    result = Generator([("šroubek", "šroubku")], ["se zmrzkou"], "obilí").generate()
    assert (result ==
"""Jde šroubek a potká šroubek se zmrzkou.
A šroubek se zmrzkou povídá: "Šroubku, můžu jít s tebou?"
Přičemž šroubek odpoví: "Jo můžeš."

Takže jde šroubek, šroubek se zmrzkou a potká obilí.
A obilí povídá: "Šroubku, šroubku se zmrzkou, můžu jít s váma?"
Přičemž šroubek, šroubek se zmrzkou odpoví: "Ne".
Čas na smích.""")


def test_two_whos():
    result = Generator([("šroubek", "šroubku"), ("matička", "matičko")], [], "obilí").generate()
    assert (result ==
"""Jde šroubek a potká matička.
A matička povídá: "Šroubku, můžu jít s tebou?"
Přičemž šroubek odpoví: "Jo můžeš."

Takže jde šroubek, matička a potká obilí.
A obilí povídá: "Šroubku, matičko, můžu jít s váma?"
Přičemž šroubek, matička odpoví: "Ne".
Čas na smích.""")


def test_three_whos():
    result = Generator([("šroubek", "šroubku"), ("matička", "matičko"), ("hřebík", "hřebíku")], [], "obilí").generate()
    assert (result ==
"""Jde šroubek a potká matička.
A matička povídá: "Šroubku, můžu jít s tebou?"
Přičemž šroubek odpoví: "Jo můžeš."

Takže jde šroubek, matička a potká hřebík.
A hřebík povídá: "Šroubku, matičko, můžu jít s váma?"
Přičemž šroubek, matička odpoví: "Jo můžeš."

Takže jde šroubek, matička, hřebík a potká obilí.
A obilí povídá: "Šroubku, matičko, hřebíku, můžu jít s váma?"
Přičemž šroubek, matička, hřebík odpoví: "Ne".
Čas na smích.""")


def test_two_whos_one_topping():
    result = Generator([("šroubek", "šroubku"), ("matička", "matičko")], ["se zmrzkou"], "obilí").generate()
    assert (result ==
"""Jde šroubek a potká šroubek se zmrzkou.
A šroubek se zmrzkou povídá: "Šroubku, můžu jít s tebou?"
Přičemž šroubek odpoví: "Jo můžeš."

Takže jde šroubek, šroubek se zmrzkou a potká matička.
A matička povídá: "Šroubku, šroubku se zmrzkou, můžu jít s váma?"
Přičemž šroubek, šroubek se zmrzkou odpoví: "Jo můžeš."

Takže jde šroubek, šroubek se zmrzkou, matička a potká matička se zmrzkou.
A matička se zmrzkou povídá: "Šroubku, šroubku se zmrzkou, matičko, můžu jít s váma?"
Přičemž šroubek, šroubek se zmrzkou, matička odpoví: "Jo můžeš."

Takže jde šroubek, šroubek se zmrzkou, matička, matička se zmrzkou a potká obilí.
A obilí povídá: "Šroubku, šroubku se zmrzkou, matičko, matičko se zmrzkou, můžu jít s váma?"
Přičemž šroubek, šroubek se zmrzkou, matička, matička se zmrzkou odpoví: "Ne".
Čas na smích.""")


def test_with_two_toppings():
    result = Generator([("šroubek", "šroubku")], ["se zmrzkou", "s čokoládou"], "obilí").generate()
    assert (result ==
"""Jde šroubek a potká šroubek se zmrzkou.
A šroubek se zmrzkou povídá: "Šroubku, můžu jít s tebou?"
Přičemž šroubek odpoví: "Jo můžeš."

Takže jde šroubek, šroubek se zmrzkou a potká šroubek se zmrzkou s čokoládou.
A šroubek se zmrzkou s čokoládou povídá: "Šroubku, šroubku se zmrzkou, můžu jít s váma?"
Přičemž šroubek, šroubek se zmrzkou odpoví: "Jo můžeš."

Takže jde šroubek, šroubek se zmrzkou, šroubek se zmrzkou s čokoládou a potká obilí.
A obilí povídá: "Šroubku, šroubku se zmrzkou, šroubku se zmrzkou s čokoládou, můžu jít s váma?"
Přičemž šroubek, šroubek se zmrzkou, šroubek se zmrzkou s čokoládou odpoví: "Ne".
Čas na smích.""")
