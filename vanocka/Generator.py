class Generator:

    def __init__(self, whos, toppings, nope):
        super().__init__()
        self.whos = whos
        self.toppings = toppings
        self.nope = nope

    def generate(self):
        whos_options = self.generate_whos()

        result_lines = []
        whos_in_band = [whos_options[0]]
        for new_who in whos_options[1:]:
            result_lines.append(self.generate_new_line(new_who, whos_in_band))
            whos_in_band.append(new_who)

        return "\n\n".join(result_lines)

    def generate_whos(self):
        whos_options = []
        for who in self.whos:
            whos_options.append(who)
            who_with_toppings = who
            for with_what in self.toppings:
                who_with_toppings = tuple(map(lambda x: f'{x} {with_what}', who_with_toppings))
                whos_options.append(who_with_toppings)
        whos_options.append((self.nope, self.nope))
        return whos_options

    def generate_new_line(self, new_who, whos_in_band):
        band_string = ", ".join(map(lambda member: member[0], whos_in_band))
        band_string_hey_you = ", ".join(map(lambda member: member[1], whos_in_band))
        with_who = "tebou" if len(whos_in_band) < 2 else "váma"
        walks = "Jde" if len(whos_in_band) < 2 else "Takže jde"
        answer = '"Jo můžeš."' if new_who[0] is not self.nope else '"Ne".\nČas na smích.'
        return (f'{walks} {band_string} a potká {new_who[0]}.\n'
                f'A {new_who[0]} povídá: "{band_string_hey_you.capitalize()}, můžu jít s {with_who}?"\n'
                f'Přičemž {band_string} odpoví: {answer}')
