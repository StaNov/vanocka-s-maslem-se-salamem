from vanocka.Generator import Generator

print(Generator([
    ("chleba", "chlebe"),
    ("rohlík", "rohlíku"),
    ("houska", "housko"),
    ("veka", "veko"),
    ("loupák", "loupáku"),
    ("dalamánek", "dalamáneku"),
    ("mazanec", "mazanče"),
    ("vánočka", "vánočko"),
    ("pletýnka", "pletýnko"),
    ("toust", "touste")
], ["s máslem", "se salámem"], "rajče").generate())
